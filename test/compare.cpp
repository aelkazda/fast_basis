#include "polynomial.hpp"
#include <iostream>

int main()
{
    constexpr long order = 4;
    constexpr long n_var = 3;

    constexpr auto basis = PolynomialBasis<n_var, order>();
    constexpr long out_size = basis.indices[order][n_var - 1] + 1;

    double* in = new double[n_var];
    double* out = new double[static_cast<size_t>(out_size)];

    Vec<double, n_var> vec_in;
    Vec<double, out_size> vec_out;

    for (long i = 0; i < n_var; ++i)
    {
        vec_in(i) = (i + 1) / 10.0;
        in[i] = (i + 1) / 10.0;
    }

    basis(in, out);
    basis(vec_in, vec_out);

    double error = 0.0;
    for (long i = 0; i < out_size; ++i)
    {
        auto diff = (out[i] - vec_out(i));
        error += diff * diff;
    }
    std::cout << "Total difference: " << error << std::endl;
    std::cout << vec_out << std::endl;
    return 0;
}
