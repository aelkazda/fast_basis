#include "polynomial.hpp"
#include <Eigen/Dense>
#include <chrono>
#include <fstream>
#include <iostream>
int main()
{
    constexpr long order = 10;
    constexpr long n_var = 5;

    constexpr auto basis = PolynomialBasis<n_var, order>();
    constexpr long out_size = basis.indices[order][n_var - 1] + 1;
    constexpr long n = 1 << 15;

    Mat_d<n_var, -1> in = Mat_d<n_var, -1>::Random(n_var, n);
    Mat_d<out_size, -1> out = Mat_d<out_size, -1>(out_size, n).array();

    for (long j = 0; j < n_var; ++j)
    {
        in(j, 0) = (j + 1) / 10.0;
    }

    auto t0 = std::chrono::high_resolution_clock::now();

    for (long i = 0; i < n; ++i)
    {
        basis(in.col(i), out.col(i));
    }
    auto t1 = std::chrono::high_resolution_clock::now();

    double dt1 =
        std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

    std::cout << "Average time: " << dt1 / n << " µs" << std::endl;

    std::cout << in.col(0).transpose() << std::endl;
    if (out_size > 50)
    {
        std::cout << out.col(0).head(25).transpose() << " ... ";
        std::cout << out.col(0).tail(25).transpose() << std::endl;
    }
    else
    {
        std::cout << out.col(0).transpose() << std::endl;
    }

    std::ostream output(nullptr);
    output << out.sum();
    output << in.sum();
    return 0;
}
