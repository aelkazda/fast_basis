#include "polynomial.hpp"
#include "trig.hpp"
#include <chrono>
#include <fenv.h>
#include <fstream>
#include <iomanip>
#include <iostream>

using P = PolynomialBasisGenerator;
using T = TrigBasisGenerator;

int main()
{
    constexpr long order = 10;
    constexpr auto basis = ProductBasis<order, P, P, P, P, P>();
    constexpr long n_var = basis.n_var;
    constexpr long out_size = basis.indices[order][n_var - 1] + 1;
    constexpr long n = 1 << 11;

    Array<Vec_d<n_var>, -1, 1> in(n);
    Array<Vec_d<-1>, -1, 1> out(n);
    Array<Vec<Vec_d<n_var>, -1>, -1, 1> out_j(n);
    Array<Vec<Mat_d<n_var, n_var>, -1>, -1, 1> out_h(n);

    auto t = std::chrono::high_resolution_clock::now();
    for (long i = 0; i < n; ++i)
    {
        in(i) = Vec_d<n_var>::Random();
        out(i) = Vec_d<-1>(out_size);
        out_j(i) = Vec<Vec_d<n_var>, -1>(out_size);
        out_h(i) = Vec<Mat_d<n_var, n_var>, -1>(out_size);
        for (long j = 0; j < out_size; ++j)
        {
            out(i)(j) = 0.0;
            out_j(i)(j) = Vec_d<n_var>::Zero();
            out_h(i)(j) = Mat_d<n_var, n_var>::Zero();
        }
    }

    auto t0 = std::chrono::high_resolution_clock::now();
    for (long i = 0; i < n; ++i)
    {
        basis(in(i), out(i));
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    for (long i = 0; i < n; ++i)
    {
        basis(in(i), out(i), out_j(i), out_h(i));
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    for (long i = 0; i < n; ++i)
    {
        basis(in(i).data(), out(i).data(), out_j(i)(0).data(),
              out_h(i)(0).data());
    }
    auto tt2 = std::chrono::high_resolution_clock::now();

    Vec_d<-1> v(out_size);
    v.setOnes();

    auto t3 = std::chrono::high_resolution_clock::now();
    for (long i = 0; i < n; ++i)
    {
        for (long j = 1; j < out_size; ++j)
        {
            out(i)(0) += v(j) * out(i)(j);
            out_j(i)(0) += v(j) * out_j(i)(j);
            out_h(i)(0) += v(j) * out_h(i)(j);
        }
    }
    auto t4 = std::chrono::high_resolution_clock::now();

    std::cout << out(0)(0) << std::endl;
    std::cout << out_j(0)(0) << std::endl;
    std::cout << out_h(0)(0) << std::endl;

    double dt =
        std::chrono::duration_cast<std::chrono::microseconds>(t0 - t).count();
    double dt1 =
        std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();
    double dt2 =
        std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    double dtt2 =
        std::chrono::duration_cast<std::chrono::microseconds>(tt2 - t2).count();
    double dt3 =
        std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count();

    std::cout << "Average time (Initialization): " << dt / n << " µs"
              << std::endl;
    std::cout << "Average time (basis functions): " << dt1 / n << " µs"
              << std::endl;
    std::cout << "Average time (basis derivatives): " << dt2 / n << " µs"
              << std::endl;
    std::cout << "Average time (basis derivatives - C array): " << dt2 / n
              << " µs" << std::endl;
    std::cout << "Average time (basis derivatives - Eigen array): " << dtt2 / n
              << " µs" << std::endl;
    std::cout << "Average time (sum): " << dt3 / n << " µs" << std::endl;

    std::ostream output(nullptr);
    double sum = 0.;
    for (long i = 0; i < n; ++i)
    {
        sum += out(i).sum();
    }
    output << sum;

    return 0;
}
