#include "polynomial.hpp"
#include <Eigen/Dense>
#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>

int main()
{
    constexpr long order = 10;
    constexpr long n_var = 5;

    constexpr auto basis = PolynomialBasis<n_var, order>();
    constexpr long out_size = basis.indices[order][n_var - 1] + 1;
    constexpr long n = 1 << 15;

    Array<Vec_d<n_var>, -1, -1> in(1, n);
    Array<Vec_d<out_size>, -1, -1> out(1, n);

    for (long i = 0; i < n; ++i)
    {
        in(i) = Vec_d<n_var>::Random();
        out(i).setZero();
    }

    auto t0 = std::chrono::high_resolution_clock::now();

    for (long i = 0; i < n; ++i)
    {
        basis(in(i), out(i));
    }
    auto t1 = std::chrono::high_resolution_clock::now();

    double dt1 =
        std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

    std::cout << "Average time: " << dt1 / n << " µs" << std::endl;

    std::ostream output(nullptr);
    double sum = 0.;
    for (long i = 0; i < n; ++i)
    {
        sum += out(i).sum();
    }
    std::cout << sum << std::endl;

    return 0;
}
