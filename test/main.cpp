#include "polynomial.hpp"
#include <chrono>
#include <fstream>
#include <iostream>

int main()
{
    constexpr long order = 10;
    constexpr long n_var = 5;

    constexpr auto basis = PolynomialBasis<n_var, order>();
    constexpr long out_size = basis.indices[order][n_var - 1] + 1;

    constexpr long n = 1 << 15;

    std::cout << "Precomputed indices:" << std::endl;
    auto& arr = basis.indices;
    for (const auto& l : arr)
    {
        for (const auto& i : l)
        {
            std::cout << i << "\t";
        }
        std::cout << std::endl;
    }

    double* in = new double[n * n_var];
    double* out = new double[static_cast<size_t>(n * out_size)];

    for (long i = 0; i < n; ++i)
    {
        for (long j = 0; j < n_var; ++j)
        {
            in[n_var * i + j] = static_cast<double>(rand()) / RAND_MAX;
        }

        for (long j = 0; j < out_size; ++j)
        {
            out[out_size * i + j] = static_cast<double>(rand()) / RAND_MAX;
        }
    }

    auto start = std::chrono::high_resolution_clock::now();

    for (long i = 0; i < n; ++i)
    {
        basis(in + n_var * i, out + out_size * i);
    }

    auto end = std::chrono::high_resolution_clock::now();
    double microseconds =
        std::chrono::duration_cast<std::chrono::microseconds>(end - start)
            .count();

    std::cout << "Average time: " << microseconds / n << " µs" << std::endl;

    std::ostream output(nullptr);
    double tmp = 0.0;
    for (long i = 0; i < n * out_size; ++i)
    {
        tmp += out[i];
    }
    output << tmp;
    return 0;
}
