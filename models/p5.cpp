#include "polynomial.hpp"
#include "prod_basis_estimator.hpp"
#include "trig.hpp"

using P = PolynomialBasisGenerator;
using T = TrigBasisGenerator;

const long n = 5;
ProductBasisEstimator<3, 4, P, P, P, P, P> estimator(n);

std::tuple<ndarray, ndarray> batch_f(const ndarray in)
{
    return estimator.batch_f(in);
}

void compute_states(const ndarray initial_state, ndarray traj)
{
    estimator.compute_states(initial_state, traj);
}

void compute_basis(const ndarray in, ndarray out)
{
    estimator.compute_basis(in, out);
}

long n_out()
{
    return estimator.n_out;
}

void set_coeffs(const ndarray coeffs)
{
    estimator.coeffs = coeffs.data(0);
}

PYBIND11_MODULE(p5, m)
{
    m.def("f_derivs", &batch_f);
    m.def("solve", &compute_states);
    m.def("eval", &compute_basis);
    m.def("n_out", &n_out);
    m.def("set_coeffs", &set_coeffs);
}
