#include "polynomial.hpp"
#include "prod_basis_estimator.hpp"
#include "trig.hpp"

using P = PolynomialBasisGenerator;
using T = TrigBasisGenerator;

constexpr long n = 500;
ProductBasisEstimator<4, 4, P, T, P, P, P> estimator(n);
constexpr long n_out = estimator.n_out;
constexpr long n_in = estimator.n_in;
constexpr long n_u = estimator.n_u;
constexpr long n_x = n_in - n_u;

std::tuple<ndarray, ndarray> batch_f(const ndarray in)
{
    return estimator.batch_f(in);
}

Eigen::DiagonalMatrix<double, n_in> A;
Eigen::DiagonalMatrix<double, n_x> B;

ndarray solve_one(const ndarray in)
{
    auto in_ptr = in.data(0);
    ndarray out(std::vector<size_t>{n_x});
    auto out_ptr = out.mutable_data(0);
    estimator.system_f(in_ptr, out_ptr);
    return out;
}

std::tuple<ndarray, ndarray, ndarray, ndarray> batch_l(const ndarray in)
{
    auto in_ptr = in.data(0);

    ndarray jacobian_out(std::vector<size_t>{static_cast<size_t>(n), n_in});
    ndarray hessian_out(
        std::vector<size_t>{static_cast<size_t>(n), n_in, n_in});
    ndarray fjacobian_out(std::vector<size_t>{n_x});
    ndarray fhessian_out(std::vector<size_t>{n_x, n_x});

    auto jacobian_ptr = jacobian_out.mutable_data(0);
    auto hessian_ptr = hessian_out.mutable_data(0);
    auto fjacobian_ptr = fjacobian_out.mutable_data(0);
    auto fhessian_ptr = fhessian_out.mutable_data(0);

#pragma omp parallel for
    for (long i = 0; i < n; ++i)
    {
        Map<Vec_d<n_in>> jac(jacobian_ptr + n_in * i);
        Map<Mat_d<n_in, n_in>> hess(hessian_ptr + n_in * n_in * i);

        jac = A * Map<const Vec_d<n_in>>(in_ptr + n_in * i);
        hess = A;
    }

    Map<Vec_d<n_x>> jac(fjacobian_ptr);
    Map<Mat_d<n_x, n_x>> hess(fhessian_ptr);

    jac = B * Map<const Vec_d<n_x>>(in_ptr + n_in * n);
    hess = B;

    return std::make_tuple(jacobian_out, hessian_out, fjacobian_out,
                           fhessian_out);
}

ndarray cost_sequence(const ndarray traj)
{
    auto traj_ptr = traj.data();
    ndarray costs(static_cast<size_t>(n + 1));
    auto cost_ptr = costs.mutable_data();
    for (long i = 0; i < n; ++i)
    {
        Map<const Vec_d<n_in>> xu(traj_ptr + i * n_in);
        cost_ptr[i] = xu.dot(A * xu);
    }
    Map<const Vec_d<n_x>> x(traj_ptr + n * n_in);
    cost_ptr[n] = x.dot(B * x);
    return costs;
}

void compute_states(const ndarray initial_state, ndarray traj)
{
    estimator.compute_states(initial_state, traj);
}

void compute_basis(const ndarray in, ndarray out)
{
    estimator.compute_basis(in, out);
}

void compute_from_DDP_output_unbounded(
    const ndarray orig_traj, const std::vector<Ref<Vec_d<n_u>>>& feedforward,
    const std::vector<Ref<Eigen::Matrix<double, n_u, n_x>>>& feedback,
    const double step, ndarray new_traj)
{
    estimator.compute_from_DDP_output_unbounded(orig_traj, feedforward,
                                                feedback, step, new_traj);
}

void set_coeffs(const ndarray coeffs)
{
    A.diagonal() << 1., 0., 1., 1., 1.;
    A.diagonal() /= n;
    B.diagonal() << 0., 1., 1., 1.;
    estimator.coeffs = coeffs.data(0);
}

PYBIND11_MODULE(inv_pend, m)
{
    m.def("eval", &compute_basis);

    m.def("solve", &compute_states);
    m.def("solve_one", &solve_one);
    m.def("cost_seq", &cost_sequence);

    m.def("f_derivs", &batch_f);
    m.def("l_derivs", &batch_l);

    m.def("from_ddp_output_unbounded", &compute_from_DDP_output_unbounded);

    m.def("set_coeffs", &set_coeffs);

    m.attr("n_out") = n_out;
    m.attr("state_dim") = n_x;
    m.attr("control_dim") = n_u;
}
