#ifndef PROD_BASIS_ESTIMATOR_HPP_IFCQJT0M
#define PROD_BASIS_ESTIMATOR_HPP_IFCQJT0M

#include "product_basis.hpp"
#include <pybind11/eigen.h>
#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;
using ndarray = py::array_t<double, py::array::c_style>;

template <long order, class... BasisGenerators>
constexpr static ProductBasis<order, BasisGenerators...> BASIS;

template <long n_x, long order, class... BasisGenerators>
struct ProductBasisEstimator
{
    constexpr static long n_in = sizeof...(BasisGenerators);
    constexpr static long n_out = BASIS<order, BasisGenerators...>.n_out;
    constexpr static long n_u = n_in - n_x;

    const long n;
    const double* coeffs;

    double* const output_buffer;
    double* const jac_buffer;
    double* const hess_buffer;

    ProductBasisEstimator(long _n, double* _coeffs)
        : n(_n), coeffs(_coeffs),
          output_buffer(new double[static_cast<size_t>(_n * n_out)]),
          jac_buffer(new double[static_cast<size_t>(_n * n_out * n_in)]),
          hess_buffer(new double[static_cast<size_t>(_n * n_out * n_in * n_in)])
    {
        std::fill_n(output_buffer, n * n_out, 0.0);
        std::fill_n(jac_buffer, n * n_out * n_in, 0.0);
        std::fill_n(hess_buffer, n * n_out * n_in * n_in, 0.0);
    }

    ProductBasisEstimator(long _n)
        : n(_n), output_buffer(new double[static_cast<size_t>(_n * n_out)]),
          jac_buffer(new double[static_cast<size_t>(_n * n_out * n_in)]),
          hess_buffer(new double[static_cast<size_t>(_n * n_out * n_in * n_in)])
    {
        std::fill_n(output_buffer, n * n_out, 0.0);
        std::fill_n(jac_buffer, n * n_out * n_in, 0.0);
        std::fill_n(hess_buffer, n * n_out * n_in * n_in, 0.0);
    }

    ~ProductBasisEstimator()
    {
        delete[] output_buffer;
        delete[] jac_buffer;
        delete[] hess_buffer;
    }

    void system_f(const double* in, double* out) const
    {
        BASIS<order, BasisGenerators...>(in, output_buffer);

        Map<Vec_d<n_x>> out_mat(out);
        out_mat = Map<const Vec_d<n_x>>(in) +
                  Map<const Eigen::Matrix<double, n_x, n_out, Eigen::RowMajor>>(
                      coeffs) *
                      Map<Vec_d<n_out>>(output_buffer);
    }

    void compute_states(const ndarray initial_state, ndarray traj) const
    {
        auto traj_ptr = traj.data(0);
        auto traj_writable_ptr = traj.mutable_data(0);
        auto init_ptr = initial_state.data(0);

        std::copy_n(init_ptr, n_x, traj_writable_ptr);
        for (long i = 0; i < n; ++i)
        {
            system_f(traj_ptr + n_in * i, traj_writable_ptr + n_in * (i + 1));
        }
    }

    void compute_basis(const ndarray in, ndarray out) const
    {
        auto in_ptr = in.data(0);
        auto out_ptr = out.mutable_data(0);
        auto len = in.shape(0);

#pragma omp parallel for
        for (long i = 0; i < len; ++i)
        {
            BASIS<order, BasisGenerators...>(in_ptr + n_in * i,
                                             out_ptr + n_out * i);
        }
    }

    void compute_from_DDP_output(
        const ndarray orig_traj, const Ref<Vec_d<n_u>> lo,
        const Ref<Vec_d<n_u>> hi,
        const std::vector<Ref<Vec_d<n_u>>>& feedforward,
        const std::vector<Ref<Eigen::Matrix<double, n_u, n_x>>>& feedback,
        const double step, ndarray new_traj) const
    {
        auto traj_ptr = orig_traj.data(0);
        auto traj_new_ptr = new_traj.data(0);
        auto traj_new_next_ptr = new_traj.mutable_data(0) + n_in;

        std::copy(traj_ptr, traj_ptr + n_x, traj_new_ptr);
        for (long i = 0; i < n; ++i)
        {
            Map<Vec_d<n_u>> u_new(traj_new_ptr + (n_x + i * (n_x + n_u)));
            u_new =
                (Map<const Vec_d<n_u>>(traj_ptr + (n_x + i * (n_x + n_u))) +
                 step * feedforward[i] +
                 feedback[i] *
                     (Map<const Vec_d<n_x>>(traj_new_ptr + i * (n_x + n_u)) -
                      Map<const Vec_d<n_x>>(traj_ptr + (i * (n_x + n_u)))))
                    .cwiseMin(hi)
                    .cwiseMax(lo);
            system_f(traj_new_ptr + i * (n_x + n_u),
                     traj_new_next_ptr + i * (n_x + n_u));
        }
    }
    void compute_from_DDP_output_unbounded(
        const ndarray orig_traj,
        const std::vector<Ref<Vec_d<n_u>>>& feedforward,
        const std::vector<Ref<Eigen::Matrix<double, n_u, n_x>>>& feedback,
        const double step, ndarray new_traj) const
    {
        auto traj_ptr = orig_traj.data(0);
        auto traj_new_ptr = new_traj.data(0);
        auto traj_new_writable_ptr = new_traj.mutable_data(0);

        std::copy(traj_ptr, traj_ptr + n_x, traj_new_writable_ptr);
        for (long i = 0; i < n; ++i)
        {
            Map<Vec_d<n_u>> u_new(traj_new_writable_ptr +
                                  (n_x + i * (n_x + n_u)));
            u_new = Map<const Vec_d<n_u>>(traj_ptr + (n_x + i * (n_x + n_u))) +
                    step * feedforward[i] +
                    feedback[i] *
                        (Map<const Vec_d<n_x>>(traj_new_ptr + i * (n_x + n_u)) -
                         Map<const Vec_d<n_x>>(traj_ptr + (i * (n_x + n_u))));
            system_f(traj_new_ptr + i * (n_x + n_u),
                     traj_new_writable_ptr + (i + 1) * (n_x + n_u));
        }
    }

    std::tuple<ndarray, ndarray> batch_f(const ndarray in) const
    {
        auto in_ptr = in.data(0);

        ndarray jacobian_out(
            std::vector<size_t>{static_cast<size_t>(n), n_x, n_in});
        ndarray hessian_out(
            std::vector<size_t>{static_cast<size_t>(n), n_x, n_in, n_in});

        auto jacobian_ptr = jacobian_out.mutable_data(0);
        auto hessian_ptr = hessian_out.mutable_data(0);
#pragma omp parallel for
        for (long i = 0; i < n; ++i)
        {
            BASIS<order, BasisGenerators...>(
                in_ptr + i * n_in, output_buffer + i * n_out,
                jac_buffer + i * n_out * n_in,
                hess_buffer + i * n_out * n_in * n_in);
            for (long j = 0; j < n_x; ++j)
            {
                Map<Vec_d<n_in>> jac(jacobian_ptr + (i * n_x + j) * n_in);
                Map<Vec_d<n_in * n_in>> hess(hessian_ptr +
                                             (i * n_x + j) * n_in * n_in);

                jac = Map<const Mat_d<n_in, n_out>>(jac_buffer +
                                                    i * n_out * n_in) *
                      Map<const Vec_d<n_out>>(coeffs + j * n_out);
                jac(j) += 1.0;

                hess = Map<const Mat_d<n_in * n_in, n_out>>(
                           hess_buffer + i * n_out * n_in * n_in) *
                       Map<const Vec_d<n_out>>(coeffs + j * n_out);

                Map<Mat_d<n_in, n_in>> hess_mat(hessian_ptr +
                                                (i * n_x + j) * n_in * n_in);
                hess_mat = (hess_mat + hess_mat.transpose()).eval();
            }
        }
        return std::make_tuple(jacobian_out, hessian_out);
    }
};

#endif /* end of include guard: PROD_BASIS_ESTIMATOR_HPP_IFCQJT0M */
