#ifndef TRIG_HPP_AVMGMEB6
#define TRIG_HPP_AVMGMEB6

#include <cmath>

struct TrigBasisGenerator
{
    template <long n> static void f(double x, double out[n])
    {
        constexpr long m = n / 2;
        for (long i = 0; i < m; ++i)
        {
            out[2 * i] = sin((i + 1) * x);
            out[2 * i + 1] = cos((i + 1) * x);
        }

        if constexpr (n % 2 == 1)
        {
            out[n - 1] = sin((m + 1) * x);
        }
    }

    template <long n>
    static void derivs(double x, double out[n], double out_j[n],
                       double out_h[n])
    {
        constexpr long m = n / 2;
        for (long i = 0; i < m; ++i)
        {
            out[2 * i] = sin((i + 1) * x);
            out[2 * i + 1] = cos((i + 1) * x);

            out_j[2 * i] = out[2 * i + 1] * (i + 1);
            out_j[2 * i + 1] = -out[2 * i] * (i + 1);

            out_h[2 * i] = out_j[2 * i + 1] * (i + 1);
            out_h[2 * i + 1] = -out_j[2 * i] * (i + 1);
        }

        if constexpr (n % 2 == 1)
        {
            out[n - 1] = sin((m + 1) * x);
            out_j[n - 1] = cos((m + 1) * x) * (m + 1);
            out_h[n - 1] = out[n - 1] * (-(m + 1) * (m + 1));
        }
    }
};

#endif /* end of include guard: TRIG_HPP_AVMGMEB6 */
