#ifndef POLYNOMIAL_HPP_SEVI8JRG
#define POLYNOMIAL_HPP_SEVI8JRG

#include "product_basis.hpp"
#include <Eigen/Dense>

struct PolynomialBasisGenerator
{
    template <long n> static void f(double x, double out[n])
    {
        out[0] = x;
        for (long i = 0; i < n - 1; ++i)
        {
            out[i + 1] = out[i] * x;
        }
    }

    template <long n>
    static void derivs(double x, double out[n], double out_j[n],
                       double out_h[n])
    {
        out[0] = x;
        out_j[0] = 1.0;
        out_h[0] = 0.0;
        if constexpr (n > 1)
        {
            out[1] = x * x;
            out_j[1] = x + x;
            out_h[1] = 2.0;
        }
        for (long i = 1; i < n - 1; ++i)
        {
            out[i + 1] = out[i] * x;
            out_j[i + 1] = out[i] * (i + 2);
            out_h[i + 1] = out[i - 1] * (i + 2) * (i + 1);
        }
    }
};

template <long n_var, long order> struct PolynomialBasis
{
    constexpr const static auto& indices =
        PRODUCT_BASIS_INDICES<n_var, order>.indices;

    void operator()(const double* input, double* output) const;

    void operator()(const Ref<Vec_d<n_var>> input,
                    Ref<Vec_d<indices[order][n_var - 1] + 1>> output) const;
};

template <long n_var, long order>
void PolynomialBasis<n_var, order>::operator()(const double* input,
                                               double* output) const
{
    output[0] = 1.0;

    for (long q = 1; q < order + 1; ++q)
    {
        for (long i = 0; i < n_var - 1; ++i)
        {
            for (long k = 0; k < indices[q][i + 1] - indices[q][i]; ++k)
            {
                output[indices[q][i] + k] =
                    output[indices[q - 1][i] + k] * input[i];
            }
        }
        output[indices[q][n_var - 1]] =
            output[indices[q - 1][n_var - 1]] * input[n_var - 1];
    }
}

template <long n_var, long order>
void PolynomialBasis<n_var, order>::
operator()(const Ref<Vec_d<n_var>> input,
           Ref<Vec_d<indices[order][n_var - 1] + 1>> output) const
{
    output(0) = 1.0;

    for (long q = 1; q < order + 1; ++q)
    {
        for (long i = 0; i < n_var - 1; ++i)
        {
            for (long k = 0; k < indices[q][i + 1] - indices[q][i]; ++k)
            {
                output(indices[q][i] + k) =
                    output(indices[q - 1][i] + k) * input(i);
            }
        }
        output(indices[q][n_var - 1]) =
            output(indices[q - 1][n_var - 1]) * input(n_var - 1);
    }
}

#endif /* end of include guard: POLYNOMIAL_HPP_SEVI8JRG */
