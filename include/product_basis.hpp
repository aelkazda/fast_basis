#ifndef PRODUCT_BASIS_HPP_EOIBUYAI
#define PRODUCT_BASIS_HPP_EOIBUYAI

#include <Eigen/Dense>

using Eigen::Ref;
template <typename Scalar, long rows, long cols>
using Mat = Eigen::Matrix<Scalar, rows, cols, Eigen::ColMajor>;
template <long rows, long cols> using Mat_d = Mat<double, rows, cols>;

template <typename Scalar, long size> using Vec = Mat<Scalar, size, 1>;
template <long size> using Vec_d = Vec<double, size>;

template <typename Scalar, long rows, long cols>
using Array = Eigen::Array<Scalar, rows, cols, Eigen::ColMajor>;
template <long rows, long cols> using Array_d = Array<double, rows, cols>;

using Eigen::Map;

template <long n_var, long order> struct ProductBasisIndices
{
    long indices[order + 1][n_var];
    constexpr ProductBasisIndices() : indices()
    {
        for (long i = 0; i < n_var; ++i)
        {
            indices[0][i] = 0;
        }

        for (long q = 0; q < order; ++q)
        {
            indices[q + 1][0] = indices[q][n_var - 1] + 1;

            for (long i = 0; i < n_var - 1; ++i)
            {
                indices[q + 1][i + 1] =
                    indices[q + 1][i] + indices[q + 1][0] - indices[q][i];
            }
        }
    }
};

template <long order, class... BasisGenerators> struct ProductBasisFunctions;

template <long order, class BasisGenerator>
struct ProductBasisFunctions<order, BasisGenerator>
{
    void (*functions[1])(double, double[order]);
    void (*derivs[1])(double, double[order], double[order], double[order]);
    constexpr ProductBasisFunctions() : functions(), derivs()
    {
        functions[0] = BasisGenerator::template f<order>;
        derivs[0] = BasisGenerator::template derivs<order>;
    }
};

template <long order, class FirstBasisGenerator, class... OtherBasisGenerators>
struct ProductBasisFunctions<order, FirstBasisGenerator,
                             OtherBasisGenerators...>
{
    void (*functions[1 + sizeof...(OtherBasisGenerators)])(double,
                                                           double[order]);
    void (*derivs[1 + sizeof...(OtherBasisGenerators)])(double, double[order],
                                                        double[order],
                                                        double[order]);
    constexpr ProductBasisFunctions() : functions(), derivs()
    {
        functions[0] = FirstBasisGenerator::template f<order>;
        derivs[0] = FirstBasisGenerator::template derivs<order>;
        auto other_basis =
            ProductBasisFunctions<order, OtherBasisGenerators...>();
        auto f_arr = other_basis.functions;
        auto der_arr = other_basis.derivs;
        for (size_t i = 0; i < sizeof...(OtherBasisGenerators); ++i)
        {
            derivs[1 + i] = der_arr[i];
            functions[1 + i] = f_arr[i];
        }
    }
};

template <long n_var, long order>
constexpr const static ProductBasisIndices<n_var, order> PRODUCT_BASIS_INDICES;
template <long order, class... BasisGenerators>
constexpr const static ProductBasisFunctions<order, BasisGenerators...>
    PRODUCT_BASIS_FUNCTIONS;

template <long order, class... BasisGenerators> struct ProductBasis
{
    constexpr const static long n_var = sizeof...(BasisGenerators);
    constexpr const static auto& indices =
        PRODUCT_BASIS_INDICES<n_var, order>.indices;
    constexpr const static auto& functions =
        PRODUCT_BASIS_FUNCTIONS<order, BasisGenerators...>.functions;
    constexpr const static auto& derivs =
        PRODUCT_BASIS_FUNCTIONS<order, BasisGenerators...>.derivs;
    constexpr const static long n_out = indices[order][n_var - 1] + 1;

    void operator()(const double* input, double* output) const;
    void operator()(long n, double* input, double* output) const
    {
#pragma omp parallel for
        for (long i = 0; i < n; ++i)
        {
            this->operator()(input + i * n_var, output + i * n_out);
        }
    }
    void operator()(Ref<Vec_d<n_var>> input, Ref<Vec_d<-1>> output) const
    {
        this->operator()(input.data(), output.data());
    }

    // Hessians are h + h^T, where h are the matrices in output_h
    void operator()(const double* input, double* output,
                    double (*output_j)[n_var],
                    double (*output_h)[n_var][n_var]) const;
    void operator()(const double* input, double* output, double* output_j,
                    double* output_h) const;
    void operator()(const Ref<Vec_d<n_var>> input, Ref<Vec_d<-1>> output,
                    Ref<Vec<Vec_d<n_var>, -1>> output_j,
                    Ref<Vec<Mat_d<n_var, n_var>, -1>> output_h) const;
};

template <long order, class... BasisGenerators>
void ProductBasis<order, BasisGenerators...>::operator()(const double* input,
                                                         double* output) const
{
    output[0] = 1.0;

    double values[n_var][order];
    for (long i = 0; i < n_var; ++i)
    {
        functions[i](input[i], values[i]);
    }

    for (long i = 0; i < n_var; ++i)
    {
        for (long q = 1; q <= order; ++q)
        {
            output[indices[q][i]] = values[i][q - 1];
        }
    }

    for (long q = 2; q <= order; ++q)
    {
        for (long i = 0; i < n_var - 1; ++i)
        {
            long j = indices[q][i] + 1;
            for (long p = 1; p < q; ++p)
            {
                for (long k = 0; k < indices[p + 1][0] - indices[p][i + 1]; ++k)
                {
                    output[j + k] =
                        values[i][q - p - 1] * output[indices[p][i + 1] + k];
                }
                j += indices[p + 1][0] - indices[p][i + 1];
            }
        }
    }
}

template <long order, class... BasisGenerators>
void ProductBasis<order, BasisGenerators...>::
operator()(const double* input, double* output, double* output_j,
           double* output_h) const
{
    output[0] = 1.0;
    for (long i = 0; i < n_var; ++i)
    {
        output_j[i] = 0.0;
        for (long j = 0; j < n_var; ++j)
        {
            output_h[i + n_var * j] = 0.0;
        }
    }

    double values[n_var][order];
    double values_j[n_var][order];
    double values_h[n_var][order];

    for (long i = 0; i < n_var; ++i)
    {
        derivs[i](input[i], values[i], values_j[i], values_h[i]);
    }

    for (long i = 0; i < n_var; ++i)
    {
        for (long q = 1; q <= order; ++q)
        {
            output[indices[q][i]] = values[i][q - 1];
            output_j[indices[q][i] * n_var + i] = values_j[i][q - 1];
            output_h[indices[q][i] * n_var * n_var + i + n_var * i] =
                0.5 * values_h[i][q - 1];
        }
    }

    for (long q = 2; q <= order; ++q)
    {
        for (long i = 0; i < n_var - 1; ++i)
        {
            long j = indices[q][i] + 1;
            for (long p = 1; p < q; ++p)
            {
                for (long k = 0; k < indices[p + 1][0] - indices[p][i + 1]; ++k)
                {
                    output[j + k] =
                        values[i][q - p - 1] * output[indices[p][i + 1] + k];

                    Map<Vec_d<n_var>> out_j(output_j + (j + k) * n_var);
                    Map<Vec_d<n_var>> out_j_prev(
                        output_j + (indices[p][i + 1] + k) * n_var);
                    out_j = values[i][q - p - 1] * out_j_prev;
                    out_j(i) =
                        values_j[i][q - p - 1] * output[indices[p][i + 1] + k];

                    Map<Mat_d<n_var, n_var>> out_h(output_h +
                                                   (j + k) * n_var * n_var);
                    Map<Mat_d<n_var, n_var>> out_h_prev(
                        output_h + (indices[p][i + 1] + k) * n_var * n_var);
                    out_h = values[i][q - p - 1] * out_h_prev;
                    out_h.col(i) = values_j[i][q - p - 1] * out_j_prev;
                    out_h(i, i) = 0.5 * values_h[i][q - p - 1] *
                                  output[indices[p][i + 1] + k];
                }
                j += indices[p + 1][0] - indices[p][i + 1];
            }
        }
    }
}

template <long order, class... BasisGenerators>
void ProductBasis<order, BasisGenerators...>::
operator()(const Ref<Vec_d<n_var>> input, Ref<Vec_d<-1>> output,
           Ref<Vec<Vec_d<n_var>, -1>> output_j,
           Ref<Vec<Mat_d<n_var, n_var>, -1>> output_h) const
{
    output(0) = 1.0;
    output_j(0).setZero();
    output_h(0).setZero();

    double values[n_var][order];
    double values_j[n_var][order];
    double values_h[n_var][order];
    for (long i = 0; i < n_var; ++i)
    {
        derivs[i](input(i), values[i], values_j[i], values_h[i]);
    }

    for (long i = 0; i < n_var; ++i)
    {
        for (long q = 1; q <= order; ++q)
        {
            output(indices[q][i]) = values[i][q - 1];
            output_j(indices[q][i])(i) = values_j[i][q - 1];
            output_h(indices[q][i])(i, i) = 0.5 * values_h[i][q - 1];
        }
    }

    for (long q = 2; q <= order; ++q)
    {
        for (long i = 0; i < n_var - 1; ++i)
        {
            long j = indices[q][i] + 1;
            for (long p = 1; p < q; ++p)
            {
                for (long k = 0; k < indices[p + 1][0] - indices[p][i + 1]; ++k)
                {
                    output(j + k) =
                        values[i][q - p - 1] * output(indices[p][i + 1] + k);

                    output_j(j + k) =
                        values[i][q - p - 1] * output_j(indices[p][i + 1] + k);
                    output_j(j + k)(i) =
                        values_j[i][q - p - 1] * output(indices[p][i + 1] + k);

                    output_h(j + k) =
                        values[i][q - p - 1] * output_h(indices[p][i + 1] + k);
                    output_h(j + k).col(i) = values_j[i][q - p - 1] *
                                             output_j(indices[p][i + 1] + k);
                    output_h(j + k)(i, i) = 0.5 * values_h[i][q - p - 1] *
                                            output(indices[p][i + 1] + k);
                }
                j += indices[p + 1][0] - indices[p][i + 1];
            }
        }
    }
}

#endif /* end of include guard: PRODUCT_BASIS_HPP_EOIBUYAI */
